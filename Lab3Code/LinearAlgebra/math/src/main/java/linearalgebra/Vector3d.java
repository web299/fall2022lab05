//Seyed Amirreza Mojtahedi 2133044
package linearalgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        double result = Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2);
        result = Math.sqrt(result);
        return result;
    }

    public double dotProduct(Vector3d myVector) {
        return this.x * myVector.getX() + this.y * myVector.getY() + this.z * myVector.getZ();
    }

    public Vector3d add(Vector3d myVector) {
        double x = this.x + myVector.getX();
        double y = this.y + myVector.getY();
        double z = this.z + myVector.getZ();
        Vector3d newVector = new Vector3d(x, y, z);
        return newVector;
    }

    public static void main(String[] args) {
        Vector3d myVector1 = new Vector3d(1, 2, 3);
        Vector3d myVector2 = new Vector3d(4, 5, 6);
        System.out.println("The magnitude is: " + myVector1.magnitude());
        // Should be 3.7...
        System.out.println("The dot product is: " + myVector1.dotProduct(myVector2));
        // Should be 32
        Vector3d myVector3 = myVector1.add(myVector2);
        System.out.println("X is: " + myVector3.getX());
        // 5
        System.out.println("Y is: " + myVector3.getY());
        // 7
        System.out.println("Z is: " + myVector3.getZ());
        // 9
    }
}