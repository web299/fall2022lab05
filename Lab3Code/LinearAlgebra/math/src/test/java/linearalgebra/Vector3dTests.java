//Seyed Amirreza Mojtahedi 2133044
package linearalgebra;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;

public class Vector3dTests {
    final double TOLERANCE=0.00001;
    @Test
    public void testGet(){
        Vector3d myVector=new Vector3d(1, 2, 3);
        assertEquals(1,myVector.getX(),TOLERANCE);
        assertEquals(2,myVector.getY(),TOLERANCE);
        assertEquals(3,myVector.getZ(),TOLERANCE);
    }
  
    @Test
    public void testMagnitude(){
        Vector3d myVector=new Vector3d(1, 2, 3);
        assertEquals(3.741657,myVector.magnitude(),TOLERANCE);
    }
    @Test
    public void testDotProduct(){
        Vector3d myVector1=new Vector3d(1, 2, 3);
        Vector3d myVector2=new Vector3d(4, 5, 6);
        assertEquals(32,myVector1.dotProduct(myVector2),TOLERANCE);
    }
    @Test
    public void testAdd(){
        Vector3d myVector1=new Vector3d(1, 2, 3);
        Vector3d myVector2=new Vector3d(4, 5, 6);
        assertEquals(5,myVector1.add(myVector2).getX(),TOLERANCE);
        assertEquals(7,myVector1.add(myVector2).getY(),TOLERANCE);
        assertEquals(9,myVector1.add(myVector2).getZ(),TOLERANCE);

    }
}
